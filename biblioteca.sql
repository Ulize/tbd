/* Ejercicio 1 */

CREATE DATABASE IF NOT EXISTS Biblioteca;

USE Biblioteca;

DROP TABLE IF EXISTS Escribe;
DROP TABLE IF EXISTS Autor;
DROP TABLE IF EXISTS Libro;

CREATE TABLE Autor (
  ID			INT			NOT NULL	AUTO_INCREMENT,
  nombre		VARCHAR(30)	NOT NULL,
  apellido		VARCHAR(30)	NOT NULL,
  nacionalidad	VARCHAR(20)	NULL,
  residencia	VARCHAR(30)	NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE Libro (
  ISBN		BIGINT		NOT NULL,
  titulo	VARCHAR(30)	NOT NULL,
  editorial	VARCHAR(30)	NULL,
  precio	FLOAT		NULL,
  PRIMARY KEY (ISBN)
);

CREATE TABLE Escribe (
  ID	INT		NOT NULL,
  año	INT		NULL,
  ISBN	BIGINT	NOT NULL,
  PRIMARY KEY (ID, ISBN),
  FOREIGN KEY (ID) REFERENCES Autor(ID) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (ISBN)  REFERENCES Libro(ISBN) ON DELETE CASCADE ON UPDATE CASCADE
);

/* Ejercicio 2 */

CREATE INDEX titulo_index on Libro(titulo);
CREATE INDEX apellido_index on Autor(apellido);

/* Ejercicio 3 */

-- a)
INSERT INTO Autor VALUES(DEFAULT, 'Abelardo', 'Castillo', 'Argentina', 'San Pedro');
INSERT INTO Autor VALUES(DEFAULT, 'Roberto', 'Fontanarrosa', 'Argentina', 'Rosario');
INSERT INTO Autor VALUES(DEFAULT, 'Jorge', 'Borges', 'Argentina', 'Buenos Aires');
INSERT INTO Autor VALUES(DEFAULT, 'Carmen', 'Laforet', 'Española', 'Madrid');
INSERT INTO Autor VALUES(DEFAULT, 'Ana Lydia', 'Vega', 'Puertorriqueña', 'San Juan');
INSERT INTO Autor VALUES(DEFAULT, 'Jorge', 'Kaczewer', 'Paraguaya', 'Asunción');

INSERT INTO Libro VALUES(9788494118210, 'El que tiene sed',  'Carpe Noctem', 8915.00);
INSERT INTO Libro VALUES(9789504932154, 'INODORO PEREYRA 1', 'PLANETA EDITORIAL', 1800.00);
INSERT INTO Libro VALUES(9789875666474, 'Ficciones', 'DEBOLSILLO', 3149.00);
INSERT INTO Libro VALUES(9788423342792, 'Nada', 'Destino', 5149.00);
INSERT INTO Libro VALUES(9789876091664, 'Amenaza Transgenica', 'NUEVO EXTREMO', 160.00);

INSERT INTO Escribe VALUES((SELECT ID FROM Autor WHERE nombre='Abelardo' AND apellido='Castillo'), 1985, 9788494118210);
INSERT INTO Escribe VALUES((SELECT ID FROM Autor WHERE nombre='Roberto' AND apellido='Fontanarrosa'), 2013, 9789504932154);
INSERT INTO Escribe VALUES((SELECT ID FROM Autor WHERE nombre='Jorge' AND apellido='Borges'), 1998, 9789875666474);
INSERT INTO Escribe VALUES((SELECT ID FROM Autor WHERE nombre='Carmen' AND apellido='Laforet'), 1945, 9788423342792);
INSERT INTO Escribe VALUES((SELECT ID FROM Autor WHERE nombre='Ana Lydia' AND apellido='Vega'), 1945, 9788423342792);
INSERT INTO Escribe VALUES((SELECT ID FROM Autor WHERE nombre='Jorge' AND apellido='Kaczewer'), 2009, 9789876091664);

-- b)
UPDATE Autor 
SET residencia = 'Buenos Aires' 
WHERE nombre = 'Abelardo' AND apellido = 'Castillo';

-- c)
UPDATE Libro 
SET precio = precio * 1.20
WHERE precio < 200 
AND ISBN IN (SELECT ISBN FROM Escribe, Autor 
			 WHERE nacionalidad <> 'Argentina'
			 AND Escribe.ID = Autor.ID);

UPDATE Libro 
SET precio = precio * 1.10
WHERE precio >= 200
AND ISBN IN (SELECT ISBN FROM Escribe, Autor 
				WHERE nacionalidad <> 'Argentina'
				AND Escribe.ID = Autor.ID);

-- d)
DELETE FROM Libro
WHERE EXISTS (SELECT * FROM Escribe 
			   WHERE año = 1998 
			   AND Escribe.ISBN = Libro.ISBN);

-- e) 
SELECT * FROM Libro
WHERE 2 = (SELECT COUNT(Autor.ID) FROM Escribe, Autor
			  WHERE Libro.ISBN = Escribe.ISBN
			  AND Escribe.ID = Autor.ID);