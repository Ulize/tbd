/* Ejercicio 4 */

-- a)
SELECT `nombre` FROM `Propietario`, `Persona`
WHERE `Propietario`.`codigo` = `Persona`.`codigo`;

-- b) 
SELECT `codigo` FROM `Inmueble`
WHERE `precio` >= 600000 AND `precio` <= 700000;

-- c)
SELECT `Persona`.`nombre` FROM `PrefiereZona`, `Persona`
WHERE `PrefiereZona`.`codigo_cliente` = `Persona`.`codigo`
AND `PrefiereZona`.`nombre_zona` = 'Norte'
AND `PrefiereZona`.`nombre_poblacion` = 'Santa Fe'
AND NOT EXISTS 
        (SELECT * FROM `PrefiereZona` `PrefiereZona2`
            WHERE `PrefiereZona2`.`codigo_cliente` = `Persona`.`codigo`
            AND ('Norte' <> `PrefiereZona2`.`nombre_zona`
            OR 'Santa Fe' <> `PrefiereZona2`.`nombre_poblacion`));

-- d)
SELECT `Persona`.`nombre` FROM `Persona`
WHERE EXISTS (
    SELECT * FROM `Cliente`, `PrefiereZona`
    WHERE `Cliente`.`vendedor` = `Persona`.`codigo` AND
          `Cliente`.`codigo` = `PrefiereZona`.`codigo_cliente` AND
          `PrefiereZona`.`nombre_poblacion` = "Rosario" AND
          `PrefiereZona`.`nombre_zona` = "Centro"
  );

-- e)
SELECT `Inmueble`.`nombre_zona`, COUNT(*) AS `Numero de inmuebles en venta`, AVG(`Inmueble`.`precio`) AS `Valor promedio` FROM `Inmueble`
WHERE `Inmueble`.`nombre_poblacion` = "Rosario"
GROUP BY `Inmueble`.`nombre_zona`;

-- f)
SELECT `Persona`.`nombre` FROM `Persona`
WHERE NOT EXISTS (
  SELECT * FROM `Zona`
  WHERE `Zona`.`nombre_poblacion` = "Santa Fe" AND
    `Zona`.`nombre_zona` NOT IN (
      SELECT `PrefiereZona`.`nombre_zona` FROM `PrefiereZona`
      WHERE `PrefiereZona`.`codigo_cliente` = `Persona`.`codigo` AND
            `PrefiereZona`.`nombre_poblacion` = "Santa Fe"
    )
);

-- g)
SELECT MONTH(`Visitas`.`fecha_hora`) AS `Mes del año`, COUNT(*) AS `Cantidad de visitas`
FROM `Visitas`
WHERE YEAR(`Visitas`.`fecha_hora`) = YEAR(CURRENT_DATE())
GROUP BY 1
ORDER BY 1 ASC;
